import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {StoreService} from "./store.service";
import {Router} from "@angular/router";
import {Observable, throwError} from "rxjs";
import {ActivityReport} from "../model/activity.report";

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  constructor(private http: HttpClient, private auth: AuthService, private store: StoreService, private router: Router) {
  }

  fetchActivityReport(): Observable<ActivityReport[]> {
    return this.http.get<ActivityReport[]>('api/report');
  }

}
